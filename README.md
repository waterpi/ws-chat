# ws-chat

#### 介绍
一款基于springboot+websoket的即时聊天系统，包含了群聊、单聊以及音乐播放等功能

#### 示例图
![输入图片说明](https://foruda.gitee.com/images/1702631812374492542/57903063_5407895.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1702631834035006128/99973054_5407895.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1702631898826821932/92c643a9_5407895.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1702631915154742030/e0d075c7_5407895.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1702631927720272771/97034b00_5407895.png "屏幕截图")


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
