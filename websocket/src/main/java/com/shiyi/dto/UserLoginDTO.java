package com.shiyi.dto;

import lombok.Data;

/**
 * @author: 拾壹
 * @date: 2023/12/14
 * @describe:
 */
@Data
public class UserLoginDTO {

    private String username;

    private String password;
}
