package com.shiyi.controller;

import com.shiyi.common.R;
import com.shiyi.service.MessageService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author: 拾壹
 * @date: 2023/11/14
 * @describe:
 */
@RestController
@RequestMapping("api/message")
@RequiredArgsConstructor
public class MessageController {

    private final MessageService messageService;


    @GetMapping("index")
    public R selectMessageList(Integer userId,Integer receiverId,Integer isGroup) {
        return messageService.selectMessageList(userId,receiverId,isGroup);
    }

}
