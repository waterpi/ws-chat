package com.shiyi.controller;

import com.shiyi.common.R;
import com.shiyi.dto.UserLoginDTO;
import com.shiyi.service.FriendService;
import com.shiyi.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;


/**
 * @author: 拾壹
 * @date: 2023/11/14
 * @describe: 用户控制器
 */
@RestController
@RequestMapping("api/user")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;


    @PostMapping("login")
    public R login(@RequestBody UserLoginDTO  userLoginDTO) {
        return userService.login(userLoginDTO);
    }

    @GetMapping("search")
    public R search(String username) {
        return userService.search(username);
    }

}
