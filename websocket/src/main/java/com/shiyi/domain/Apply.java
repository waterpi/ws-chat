package com.shiyi.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author: 拾壹
 * @date: 2023/12/13
 * @describe: 好友或群申请实体类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Apply {

    /**
     * 主键id
     */
    @TableId(type= IdType.AUTO)
    private Integer id;

    /**
     *  用户id
     */
    private Integer sendId;

    /**
     *  发送用户id
     */
    private Integer receiverId;

    /**
     *  是否是群申请 1：是 2：否
     */
    private Integer isGroup;

    /**
     *  状态 1：同意 2：拒绝
     */
    private Integer status;


    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

}
