package com.shiyi.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author: 拾壹
 * @date: 2023/12/13
 * @describe:
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Message {

    /**
     * 主键id
     */
    private Integer id;

    /**
     *  发送者id
     */
    private Integer senderId;

    /**
     *  接收者id
     */
    private Integer receiverId;

    /**
     *  消息内容
     */
    private String content;


    /***
     * 消息类型
     */
    private Integer type;

    /**
     * 消息状态
     */
    private Integer status;

    /**
     * 是否群聊  0：单聊 1群聊
     */
    private Integer isGroup;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

}
