package com.shiyi.common;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author: 拾壹
 * @date: 2023/12/15
 * @describe:
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class WebsocketDTO {

    private Integer type;

    private Integer senderId;


    private Integer receiverId;

    private Object data;
}
