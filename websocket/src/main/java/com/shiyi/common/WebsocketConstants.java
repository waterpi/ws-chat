package com.shiyi.common;

/**
 * @author: 拾壹
 * @date: 2023/12/15
 * @describe:
 */
public class WebsocketConstants {

    /**
     * 私聊 群聊
     */
    public static final int CHAT_CODE = 1;

    /**
     * 消息通知类型 3系统通知
     */
    public static final int MESSAGE_SYSTEM_NOTICE = 3;


    /**
     * ping 信息
     */
    public static final int PING_MESSAGE_CODE = 6;


}
