package com.shiyi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.shiyi.domain.Groups;

/**
 * @author: 拾壹
 * @date: 2023/12/14
 * @describe:
 */
public interface GroupsService extends IService<Groups> {
}
