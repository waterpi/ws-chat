package com.shiyi.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.shiyi.common.R;
import com.shiyi.domain.Session;

/**
 * @author: 拾壹
 * @date: 2023/12/13
 * @describe:
 */
public interface SessionService extends IService<Session> {

    /**
     * 会话列表
     * @return
     */
    R selectSessionList(Integer userId);
}
