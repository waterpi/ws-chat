package com.shiyi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.shiyi.common.R;
import com.shiyi.domain.User;
import com.shiyi.dto.UserLoginDTO;

/**
 * @author: 拾壹
 * @date: 2023/12/14
 * @describe:
 */
public interface UserService extends IService<User> {

    /**
     * 登录
     * @return
     */
    R login(UserLoginDTO userLoginDTO);

    /**
     * 搜索用户
     * @param username
     * @return
     */
    R search(String username);
}
