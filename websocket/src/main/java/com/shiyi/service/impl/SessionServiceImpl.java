package com.shiyi.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.shiyi.common.R;
import com.shiyi.domain.Groups;
import com.shiyi.domain.Session;
import com.shiyi.domain.User;
import com.shiyi.mapper.GroupsMapper;
import com.shiyi.mapper.SessionMapper;
import com.shiyi.mapper.UserMapper;
import com.shiyi.service.SessionService;
import com.shiyi.utils.PageUtils;
import com.shiyi.vo.SessionListVO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;



/**
 * @author: 拾壹
 * @date: 2023/12/13
 * @describe: 好友
 */
@Service
@RequiredArgsConstructor
public class SessionServiceImpl  extends ServiceImpl<SessionMapper, Session> implements SessionService {

    private final UserMapper userMapper;
    private final GroupsMapper groupsMapper;


    @Override
    public R selectSessionList(Integer userId) {
        Page<SessionListVO> page = baseMapper.selectSessionList(new Page<>(PageUtils.getPageNo(), PageUtils.getPageSize()),userId);
        for (SessionListVO sessionListVO : page.getRecords()) {
            Integer sessionId = sessionListVO.getSessionId();
            if (sessionListVO.getType() == 1) {
              User user = userMapper.selectById(sessionId);
              sessionListVO.setAvatar(user.getAvatar());
              sessionListVO.setName(user.getNickname());
              continue;
            }
            Groups groups = groupsMapper.selectGroupsById(sessionId);
            sessionListVO.setAvatar(groups.getAvatar());
            sessionListVO.setName(groups.getName());
        }
        return R.success(page);
    }
}
