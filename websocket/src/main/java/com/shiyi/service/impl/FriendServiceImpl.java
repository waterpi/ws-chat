package com.shiyi.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.shiyi.common.R;
import com.shiyi.domain.Friend;
import com.shiyi.domain.User;
import com.shiyi.mapper.FriendMapper;
import com.shiyi.mapper.UserMapper;
import com.shiyi.service.FriendService;
import com.shiyi.utils.PageUtils;
import com.shiyi.vo.ApplyListVO;
import com.shiyi.vo.FriendListVO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @author: 拾壹
 * @date: 2023/12/13
 * @describe: 好友
 */
@Service
@RequiredArgsConstructor
public class FriendServiceImpl extends ServiceImpl<FriendMapper, Friend> implements FriendService {


    private final UserMapper userMapper;


    @Override
    public R selectFriendList(Integer userId) {
        Page<FriendListVO> page = baseMapper.selectFriendList(new Page<>(PageUtils.getPageNo(),PageUtils.getPageSize()),userId);
        for (FriendListVO friendListVO : page.getRecords()) {
            int friendId = friendListVO.getFriendId() == userId ? friendListVO.getUserId() : friendListVO.getFriendId();
            User user = userMapper.selectById(friendId);
            friendListVO.setAvatar(user.getAvatar());
            friendListVO.setNickname(user.getNickname());
            friendListVO.setFriendId(friendId);
            friendListVO.setState(user.getState());
        }
        return R.success(page);
    }
}
