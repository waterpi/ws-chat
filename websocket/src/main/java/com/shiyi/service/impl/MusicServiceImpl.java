package com.shiyi.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.shiyi.common.R;
import com.shiyi.domain.Music;
import com.shiyi.mapper.MusicMapper;
import com.shiyi.service.MusicService;
import com.shiyi.utils.PageUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


/**
 * @author: 拾壹
 * @date: 2023/12/14
 * @describe:
 */
@Service
@RequiredArgsConstructor
public class MusicServiceImpl extends ServiceImpl<MusicMapper, Music> implements MusicService {


    @Override
    public R selectMusicList() {
        Page<Music> page = baseMapper.selectPage(new Page<>(PageUtils.getPageNo(), PageUtils.getPageSize()), null);
        return R.success(page);
    }
}
