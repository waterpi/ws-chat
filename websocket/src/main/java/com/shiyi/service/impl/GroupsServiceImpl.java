package com.shiyi.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.shiyi.domain.Groups;
import com.shiyi.mapper.GroupsMapper;
import com.shiyi.service.GroupsService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @author: 拾壹
 * @date: 2023/12/14
 * @describe:
 */
@Service
@RequiredArgsConstructor
public class GroupsServiceImpl extends ServiceImpl<GroupsMapper, Groups> implements GroupsService {
}
