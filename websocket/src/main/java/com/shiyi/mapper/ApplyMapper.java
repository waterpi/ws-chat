package com.shiyi.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.shiyi.domain.Apply;
import com.shiyi.vo.ApplyListVO;
import org.apache.ibatis.annotations.Param;


/**
 * @author: 拾壹
 * @date: 2023/12/13
 * @describe:
 */
public interface ApplyMapper extends BaseMapper<Apply> {

    Page<ApplyListVO> selectApplyList(@Param("page") IPage<Object> page, @Param("userId") Integer userId);

}
