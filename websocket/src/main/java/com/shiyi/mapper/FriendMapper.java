package com.shiyi.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.shiyi.domain.Friend;
import com.shiyi.vo.FriendListVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * @author: 拾壹
 * @date: 2023/12/13
 * @describe:
 */

public interface FriendMapper extends BaseMapper<Friend> {


    Page<FriendListVO> selectFriendList(@Param("page") IPage<Object> page, @Param("userId") Integer userId);


}
