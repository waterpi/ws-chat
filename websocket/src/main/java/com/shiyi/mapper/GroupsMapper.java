package com.shiyi.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.shiyi.domain.Groups;

/**
 * @author: 拾壹
 * @date: 2023/12/13
 * @describe:
 */
public interface GroupsMapper extends BaseMapper<Groups> {


    Groups selectGroupsById(Integer groupId);
}
