package com.shiyi.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.shiyi.domain.Music;

import java.util.List;


/**
 * @author: 拾壹
 * @date: 2023/12/13
 * @describe:
 */
public interface MusicMapper extends BaseMapper<Music> {

}
