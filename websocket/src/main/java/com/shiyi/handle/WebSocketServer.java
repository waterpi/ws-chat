package com.shiyi.handle;


import cn.hutool.json.JSONUtil;
import com.shiyi.common.WebsocketConstants;
import com.shiyi.common.WebsocketDTO;
import com.shiyi.domain.Message;
import com.shiyi.service.MessageService;
import com.shiyi.utils.SpringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * @author: 拾壹
 * @date: 2023/11/13
 * @describe:
 */

@ServerEndpoint(value = "/websocket/{userId}")
@Component
public class WebSocketServer {

    private static final Logger log = LoggerFactory.getLogger(WebSocketServer.class);

    /**
     * 记录当前在线连接数
     */
    public static final Map<String, Session> sessionMap = new ConcurrentHashMap<>();

    /**
     * 连接建立成功调用的方法
     */
    @OnOpen
    public void onOpen(Session session,@PathParam("userId") String userId) {
        sessionMap.put(userId, session);
        log.info("有新用户加入，username={}, 当前在线人数为：{}", userId, sessionMap.size());
    }

    /**
     * 连接关闭调用的方法
     */
    @OnClose
    public void onClose(Session session, @PathParam("userId") String userId) {
        sessionMap.remove(userId);
        log.info("有一连接关闭，移除userId={}的用户session, 当前在线人数为：{}", userId, sessionMap.size());
    }

    /**
     * 收到客户端消息后调用的方法
     * 后台收到客户端发送过来的消息
     * onMessage 是一个消息的中转站
     * 接受 浏览器端 socket.send 发送过来的 json数据
     * @param message 客户端发送过来的消息
     */
    @OnMessage
    public void onMessage(String message, Session session, @PathParam("userId") String userId) {
        log.info("服务端收到用户userId={}的消息:{}", userId, message);
        this.sendAllMessage(message);
    }

    @OnError
    public void onError(Session session, Throwable error) {
        log.error("发生错误");
        error.printStackTrace();
    }

    /**
     * 服务端发送消息给客户端
     */
    private void sendMessage(String message, Session toSession) {
        try {
            log.info("服务端给客户端[{}]发送消息{}", toSession.getId(), message);
            toSession.getBasicRemote().sendText(message);
        } catch (Exception e) {
            log.error("服务端发送消息给客户端失败", e);
        }
    }


    /**
     * 服务端发送消息给所有客户端
     */
    public void sendNoticeMessage(WebsocketDTO websocketDTO) {
        String message = JSONUtil.toJsonStr(websocketDTO);
        try {
            Session toSession = sessionMap.get(websocketDTO.getReceiverId().toString());
            toSession.getBasicRemote().sendText(message);
            log.info("服务端给客户端[{}]发送消息{}", toSession.getId(), message);
        } catch (Exception e) {
            log.error("服务端发送消息给客户端失败", e);
        }
    }



    /**
     * 服务端发送消息给所有客户端
     */
    private void sendAllMessage(String message) {
        Message domain = JSONUtil.toBean(message, Message.class);
        MessageService messageService = SpringUtils.getBean(MessageService.class);
        messageService.insertMessage(domain);
        try {
            WebsocketDTO websocket = WebsocketDTO.builder().type(WebsocketConstants.CHAT_CODE).senderId(domain.getReceiverId()).receiverId(domain.getReceiverId()).data(message).build();
            message = JSONUtil.toJsonStr(websocket);
            if (domain.getIsGroup() == 1) {
                Session toSession = sessionMap.get(domain.getReceiverId().toString());
                if (toSession != null) {
                    toSession.getBasicRemote().sendText(message);
                }
                Session fromSession = sessionMap.get(domain.getSenderId().toString());
                fromSession.getBasicRemote().sendText(message);
                return;
            }

            for (Session session : sessionMap.values()) {
                log.info("服务端给客户端[{}]发送消息{}", session.getId(), message);
                session.getBasicRemote().sendText(message);
            }
        } catch (Exception e) {
            log.error("服务端发送消息给客户端失败", e);
        }
    }
}
