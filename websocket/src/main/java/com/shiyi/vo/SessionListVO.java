package com.shiyi.vo;

import lombok.Data;

import java.util.Date;

/**
 * @author: 拾壹
 * @date: 2023/12/14
 * @describe: 会话列表vo
 */
@Data
public class SessionListVO {

    private Integer id;

    private Integer sessionId;

    private String name;

    private String avatar;

    private Integer type;

    private String lastMessage;

    private Date lastTime;
}
