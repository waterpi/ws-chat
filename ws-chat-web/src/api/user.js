import request from '@/utils/request'

export function login(data) {
    return request({
        url: '/api/user/login',
        method: 'post',
        data
    })
}
export function search(params) {
    return request({
        url: '/api/user/search',
        method: 'get',
        params: params
    })
}
