import request from '@/utils/request'

export function getMessageList(params) {
    return request({
        url: '/api/message/index',
        method: 'get',
        params: params
    })
}

